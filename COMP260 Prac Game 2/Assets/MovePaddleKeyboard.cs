﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePaddleKeyboard : MonoBehaviour
{
    public string axisXName = "Horizontal";
    public string axisYName = "Vertical";
    public float speedx = 2.0f; // in metres/second
    public float speedy = 2.0f; // i
    private float curspeed = 0.01f;
    private Rigidbody rigidbody;


    // Start is called before the first frame update
    void Start() {
        rigidbody = GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    void Update()
    {
        // vertical axis controls up down
        float forwards = Input.GetAxis(axisYName);
        // the horizontal axis controls left/right
        float sideways = Input.GetAxis(axisXName);

        if (forwards > 0)
        {
            rigidbody.AddForce(transform.forward * speedy);
        }
        else if (forwards < 0)
        {
            rigidbody.AddForce(transform.forward * -speedy);
        }
        if (sideways > 0)
        {
            rigidbody.AddForce(transform.right * speedx);
        }
        else if (sideways < 0)
        {
            rigidbody.AddForce(transform.right * -speedx);
        }
    }
}
